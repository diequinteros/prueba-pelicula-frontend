import { RefObject, useEffect } from 'react';

const useCloseContextMenu = (
  contextMenu: RefObject<Element>,
  onClickOutside: () => void,
) => {
  useEffect(() => {
    const handleClickOutsideContextMenu = (event: MouseEvent) => {
      if (
        contextMenu.current &&
        event.target instanceof Element &&
        !contextMenu.current.contains(event.target)
      ) {
        onClickOutside();
      }
    };

    document.addEventListener('mousedown', handleClickOutsideContextMenu);

    return () => {
      document.removeEventListener('mousedown', handleClickOutsideContextMenu);
    };
  }, [contextMenu, onClickOutside]);
};

export default useCloseContextMenu;
