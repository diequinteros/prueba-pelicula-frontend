import { IResponse } from '@/types/apiResponse';
import customAxios from '@/utils/customAxios';
import handleErrors from './handleErrors';
import { IMovie } from '@/types/models';

export const getMovies = async (
  page: number,
  orderBy: 'popularity' | 'title',
  title: string,
) => {
  try {
    const response = await customAxios.get<
      IResponse<{ movies: IMovie[]; totalPages: number }>
    >(`/movies?page=${page}&orderBy=${orderBy}&title=${title}&limit=12`);
    return response.data;
  } catch (error) {
    return handleErrors(error);
  }
};

export const getMovie = async (id: string) => {
  try {
    const response = await customAxios.get<IResponse<IMovie>>(`/movies/${id}`);
    return response.data;
  } catch (error) {
    return handleErrors(error);
  }
};

export const addMovie = async (movie: IMovie['data']) => {
  try {
    const response = await customAxios.post<IResponse<IMovie>>(
      '/movies',
      movie,
    );
    return response.data;
  } catch (error) {
    return handleErrors(error);
  }
};

export const updateMovie = async (id: string, movie: IMovie['data']) => {
  try {
    const response = await customAxios.put<IResponse<IMovie>>(
      `/movies/${id}`,
      movie,
    );
    return response.data;
  } catch (error) {
    return handleErrors(error);
  }
};

export const deleteMovie = async (id: string) => {
  try {
    const response = await customAxios.delete<IResponse<undefined>>(
      `/movies/${id}`,
    );
    return response.data;
  } catch (error) {
    return handleErrors(error);
  }
};

export const likeMovie = async (id: string) => {
  try {
    const response = await customAxios.get<IResponse<undefined>>(
      `/movies/${id}`,
    );
    return response.data;
  } catch (error) {
    return handleErrors(error);
  }
};
