import { IResponse } from '@/types/apiResponse';
import customAxios from '@/utils/customAxios';
import handleErrors from './handleErrors';
import { IUser } from '@/types/models';

export const signIn = async (email: string, password: string) => {
  try {
    const response = await customAxios.post<
      IResponse<{ user: Omit<IUser, 'password' | 'salt'>; token: string }>
    >('/sign-in', {
      email,
      password,
    });
    return response.data;
  } catch (error) {
    return handleErrors(error);
  }
};
