export interface IUser {
  id: string;
  data: {
    firstName: string;
    lastName: string;
    email: string;
    role: 'admin' | 'client';
    createdAt: object;
  };
}

export interface ILike {
  id: string;
  data: {
    user: string;
    movie: string;
  };
}

export interface IMovie {
  id: string;
  data: {
    title: string;
    description: string;
    image: string;
    stock: number;
    rentPrice: number;
    salePrice: number;
    createdAt: object;
    likes: number;
  };
}

export interface IChangeHistory {
  id: string;
  data: {
    movie: string;
    user: string;
    oldValues: IMovie;
    createdAt: object;
  };
}

export interface IRentsHistory {
  id: string;
  data: {
    user: string;
    movie: string;
    rentDate: Date;
    returnDate: Date;
    createdAt: object;
  };
}

export interface ISalesHistory {
  id: string;
  data: {
    user: string;
    movie: string;
    amount: number;
    createdAt: object;
  };
}
