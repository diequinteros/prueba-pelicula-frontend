export interface IResponse<IData> {
  message: string;
  data: IData;
}
