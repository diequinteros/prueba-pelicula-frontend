import Link from 'next/link';

const Home = () => {
  return (
    <section>
      <h2 className="font-bold">Prueba peliculas</h2>
      <p>
        Esta aplicacion fue desarrollada con el objetivo de servir como prueba
        tecnica y demostrar las habilidades de{' '}
        <Link
          className="underline text-theme-dark-red"
          href="https://diegoquinteros.dev"
          target="_blank"
        >
          Diego Quinteros
        </Link>
        &nbsp;como React Developer.
      </p>
      <h3>Stack de tecnologias</h3>
      <ul className="list-disc pl-8">
        <li>React</li>
        <li>NextJS (App router)</li>
        <li>TailWindCSS</li>
        <li>React Hook Form</li>
        <li>Zod</li>
        <li>Axios</li>
        <li>Lodash</li>
        <li>React icons</li>
        <li>TypeScript</li>
        <li>ESLint</li>
        <li>Prettier</li>
      </ul>
    </section>
  );
};

export default Home;
