'use client';
import Loading from '@/components/Loading';
import MoviePreview from '@/components/MoviePreview/MoviePreview';
import Pagination from '@/components/Pagination';
import { getMovies } from '@/services/movies';
import { IMovie } from '@/types/models';
import { ROUTES } from '@/utils/constants';
import { debounce } from 'lodash';
import Link from 'next/link';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import React, { ChangeEvent, useEffect, useMemo, useState } from 'react';

const Movies = () => {
  const searchParams = useSearchParams();
  const pathname = usePathname();

  const [loading, setLoading] = useState(0);
  const [movies, setMovies] = useState<IMovie[]>([]);
  const [totalPages, setTotalPages] = useState(1);
  const page = Number(searchParams.get('page')) || 1;
  const [orderBy, setOrderBy] = useState(
    searchParams.get('orderBy') || 'title',
  );
  const [title, setTitle] = useState(searchParams.get('title') || '');
  const [error, setError] = useState<string>();

  const router = useRouter();

  const handleTitleChange = (event: ChangeEvent<HTMLInputElement>) =>
    setTitle(event.currentTarget.value);

  const handleOrderByChange = (event: ChangeEvent<HTMLSelectElement>) =>
    setOrderBy(event.currentTarget.value);

  const debouncedGetMovies = useMemo(
    () =>
      debounce(async (page: number, orderBy: string, title: string) => {
        setLoading((previousLoading) => previousLoading + 1);
        if (orderBy === 'title' || orderBy === 'popularity') {
          const response = await getMovies(page, orderBy, title);
          if ('error' in response) {
            setError(response.error);
          } else {
            setMovies(response.data.movies);
            setTotalPages(response.data.totalPages);
          }
        }
        setLoading((previousLoading) => previousLoading - 1);
      }, 500),
    [],
  );

  useEffect(() => {
    debouncedGetMovies(page, orderBy, title);
  }, [debouncedGetMovies, orderBy, page, title]);

  useEffect(() => {
    router.replace(`${pathname}?title=${title}&orderBy=${orderBy}&page=1`);
  }, [pathname, title, orderBy, router]);

  return (
    <section>
      <Loading show={loading > 0} />
      <div className="flex flex-col md:flex-row gap-6 mb-6">
        <div className="flex flex-col">
          <label htmlFor="title">Titulo</label>
          <input
            id="title"
            type="text"
            value={title}
            onChange={handleTitleChange}
          />
        </div>
        <div className="flex flex-col">
          <label htmlFor="orderBy">Ordenar por</label>
          <select id="orderBy" value={orderBy} onChange={handleOrderByChange}>
            <option value="title">Titulo</option>
            <option value="popularity">Popularidad</option>
          </select>
        </div>
      </div>
      <div className=" grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4">
        {movies.map((movie) => (
          <Link href={`${ROUTES.MOVIES}/${movie.id}`} key={movie.id}>
            <MoviePreview
              title={movie.data.title}
              likes={movie.data.likes}
              image={movie.data.image}
            />
          </Link>
        ))}
      </div>
      <div className="mt-6 flex justify-center">
        <Pagination
          totalPages={totalPages}
          currentPage={page}
          route={`${pathname}?title=${title}&orderBy=${orderBy}&page=`}
        />
      </div>
    </section>
  );
};

export default Movies;
