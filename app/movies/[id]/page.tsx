'use client';
import { PageProps } from '@/.next/types/app/page';
import Alert from '@/components/Alert';
import Loading from '@/components/Loading';
import { getMovie } from '@/services/movies';
import { IMovie } from '@/types/models';
import { retrieveAuthentication } from '@/utils/authentication';
import Image from 'next/image';
import React, { useEffect, useState } from 'react';
import { MdShop, MdShoppingCart, MdThumbUp } from 'react-icons/md';

const MovieDetails = ({ params: { id } }: PageProps) => {
  const [movie, setMovie] = useState<IMovie>();
  const [error, setError] = useState<string>();
  const [loading, setLoading] = useState(0);

  const handleAcceptErrorAlert = () => setError(undefined);

  useEffect(() => {
    const handleGetMovie = async () => {
      setLoading((previousLoading) => previousLoading + 1);
      const response = await getMovie(id);
      if ('error' in response) {
        setError(response.error);
      } else {
        setMovie(response.data);
      }
      setLoading((previousLoading) => previousLoading - 1);
    };

    handleGetMovie();
  }, [id]);

  return (
    <section>
      <Loading show={loading > 0} />
      <Alert
        title="Error"
        message={error}
        show={error !== undefined}
        onAccept={handleAcceptErrorAlert}
      />
      <div className="flex flex-col md:flex-row gap-6">
        <div className="flex-[2]">
          <Image
            className="w-full object-cover"
            src={movie?.data.image || ''}
            width={600}
            height={900}
            alt={`Movie poster of ${movie?.data.title}`}
          />
        </div>
        <div className="flex-[3]">
          <h1 className=" font-bold text-4xl mb-6">{movie?.data.title}</h1>
          <p className=" text-justify">{movie?.data.description}</p>
          <div className="my-4 flex flex-col gap-4">
            <div>
              <span>{`${movie?.data.likes} Me gusta`}</span>
            </div>
            {retrieveAuthentication() !== undefined && (
              <div className="flex flex-row gap-4">
                <button
                  title="Dar me gusta"
                  className="boton-disable aspect-square p-4 rounded-full"
                >
                  <MdThumbUp size={'1.25rem'} />
                </button>
                <button
                  title="Comprar"
                  className=" aspect-square p-4 rounded-full"
                >
                  <MdShoppingCart size={'1.25rem'} />
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
    </section>
  );
};

export default MovieDetails;
