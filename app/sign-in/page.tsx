'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import {
  FieldErrors,
  SubmitErrorHandler,
  SubmitHandler,
  useForm,
} from 'react-hook-form';
import signInSchema, { TSignInSchemaType } from './page.schema';
import Alert from '@/components/Alert';
import { useState } from 'react';
import { signIn } from '@/services/signIn';
import { useRouter } from 'next/navigation';
import { ROUTES } from '@/utils/constants';
import { persistAuthentication } from '@/utils/authentication';
import Loading from '@/components/Loading';

const SignIn = () => {
  const [loading, setLoading] = useState(0);

  const [formErrors, setFormErrors] =
    useState<FieldErrors<TSignInSchemaType>>();

  const [error, setError] = useState<string>();

  const router = useRouter();

  const { handleSubmit, register } = useForm<TSignInSchemaType>({
    resolver: zodResolver(signInSchema),
    shouldFocusError: false,
    reValidateMode: 'onBlur',
  });

  const onValid: SubmitHandler<TSignInSchemaType> = async (data) => {
    setLoading((previousLoading) => previousLoading + 1);
    const response = await signIn(data.email, data.password);
    if ('error' in response) {
      setError(response.error);
      console.log(response.error);
    } else {
      persistAuthentication({
        email: response.data.user.data.email,
        role: response.data.user.data.role,
        token: response.data.token,
      });
      router.push(ROUTES.MOVIES);
    }
    setLoading((previousLoading) => previousLoading - 1);
  };

  const onError: SubmitErrorHandler<TSignInSchemaType> = (errors) => {
    setFormErrors(errors);
  };

  const onAcceptFormErrors = () => setFormErrors(undefined);

  const onAcceptError = () => setError(undefined);

  return (
    <section>
      <Loading show={loading > 0} />
      <Alert
        title="Error"
        message={error}
        onAccept={onAcceptError}
        onClickOutside={onAcceptError}
        show={error !== undefined}
      />
      <Alert
        title="Error"
        message={
          <>
            <span>Se encontraron algunos enrrores en los campos:</span>
            <ul className=" list-disc pl-8">
              {formErrors?.email && <li>Correo electronico invalido.</li>}
              {formErrors?.password && (
                <li>Contraseña invalida. Debe contener 8 caracteres.</li>
              )}
            </ul>
          </>
        }
        onAccept={onAcceptFormErrors}
        onClickOutside={onAcceptFormErrors}
        show={formErrors !== undefined}
      />
      <div className="bg-stone-900 p-12 max-w-md m-auto pb-40 rounded-md">
        <form
          className="flex flex-col gap-4 "
          onSubmit={handleSubmit(onValid, onError)}
        >
          <h2 className="font-bold text-2xl">Inicia sesion</h2>
          <div className="flex flex-col">
            <label htmlFor="email">Correo electronico</label>
            <input {...register('email')} required />
          </div>
          <div className="flex flex-col">
            <label htmlFor="password">Contraseña</label>
            <input {...register('password')} type="password" required />
          </div>
          <button className="w-full mt-6 boton-default" type="submit">
            Iniciar sesion
          </button>
        </form>
      </div>
    </section>
  );
};

export default SignIn;
