import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import './globals.css';
import Header from '@/components/Header';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'Prueba cine',
  description:
    'Esta aplicacion se desarrollo con el objetivo de demostrar las habilidades de Diego Quinteros.',
  authors: [
    {
      name: 'Diego Quinteros',
      url: 'https://diegoquinteros.dev',
    },
  ],
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="es">
      <body
        className={`${inter.className} min-h-screen flex flex-col bg-theme-dark-gray text-white`}
      >
        <Header />
        <main className="max-w-5xl my-4 m-auto w-full flex-1 p-8">
          {children}
        </main>
      </body>
    </html>
  );
}
