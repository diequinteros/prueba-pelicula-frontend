import { z } from 'zod';

const signUpSchema = z.object({
  firstName: z.string().min(1),
  lastName: z.string().min(1),
  email: z.string().email(),
  password: z.string().min(8),
  confirmPassword: z.string().min(8),
});

export default signUpSchema;

export type TSignUpSchemaType = z.infer<typeof signUpSchema>;
