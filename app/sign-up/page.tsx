'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import {
  FieldErrors,
  SubmitErrorHandler,
  SubmitHandler,
  useForm,
} from 'react-hook-form';
import signInSchema, { TSignUpSchemaType } from './page.schema';
import Alert from '@/components/Alert';
import { useState } from 'react';
import { useRouter } from 'next/navigation';
import { ROUTES } from '@/utils/constants';
import Loading from '@/components/Loading';
import { signUp } from '@/services/users';

const SignUp = () => {
  const [loading, setLoading] = useState(0);

  const [formErrors, setFormErrors] =
    useState<FieldErrors<TSignUpSchemaType>>();

  const [error, setError] = useState<string>();

  const [signedUp, setSignedUp] = useState(false);

  const router = useRouter();

  const { handleSubmit, register } = useForm<TSignUpSchemaType>({
    resolver: zodResolver(signInSchema),
    shouldFocusError: false,
    reValidateMode: 'onBlur',
  });

  const onValid: SubmitHandler<TSignUpSchemaType> = async (data) => {
    if (data.password === data.confirmPassword) {
      setLoading((previousLoading) => previousLoading + 1);
      const response = await signUp(
        data.firstName,
        data.lastName,
        data.email,
        data.password,
      );
      if ('error' in response) {
        setError(response.error);
        console.log(response.error);
      } else {
        setSignedUp(true);
      }
      setLoading((previousLoading) => previousLoading - 1);
    } else {
      setError('La contraseña de confirmacion es diferente a la contraseña.');
    }
  };

  const onError: SubmitErrorHandler<TSignUpSchemaType> = (errors) => {
    setFormErrors(errors);
  };

  const onAcceptFormErrors = () => setFormErrors(undefined);

  const onAcceptError = () => setError(undefined);

  const onAcceptSignedUp = () => {
    setSignedUp(false);
    router.push(ROUTES.SIGNIN);
  };

  return (
    <section>
      <Loading show={loading > 0} />
      <Alert
        title="Exito"
        message={'Se registro con exito.'}
        onAccept={onAcceptSignedUp}
        onClickOutside={onAcceptSignedUp}
        show={signedUp}
      />
      <Alert
        title="Error"
        message={error}
        onAccept={onAcceptError}
        onClickOutside={onAcceptError}
        show={error !== undefined}
      />
      <Alert
        title="Error"
        message={
          <>
            <span>Se encontraron algunos enrrores en los campos:</span>
            <ul className=" list-disc pl-8">
              {formErrors?.email && <li>Correo electronico invalido.</li>}
              {formErrors?.password && (
                <li>Contraseña invalida. Debe contener 8 caracteres.</li>
              )}
            </ul>
          </>
        }
        onAccept={onAcceptFormErrors}
        onClickOutside={onAcceptFormErrors}
        show={formErrors !== undefined}
      />
      <div className="bg-stone-900 p-12 max-w-md m-auto pb-40 rounded-md">
        <form
          className="flex flex-col gap-4 "
          onSubmit={handleSubmit(onValid, onError)}
        >
          <h2 className="font-bold text-2xl">Registrate</h2>
          <div className="flex flex-col">
            <label htmlFor="firstName">Nombre</label>
            <input {...register('firstName')} required />
          </div>
          <div className="flex flex-col">
            <label htmlFor="lastName">Apellido</label>
            <input {...register('lastName')} required />
          </div>
          <div className="flex flex-col">
            <label htmlFor="email">Correo electronico</label>
            <input {...register('email')} required />
          </div>
          <div className="flex flex-col">
            <label htmlFor="password">Contraseña</label>
            <input {...register('password')} type="password" required />
          </div>
          <div className="flex flex-col">
            <label htmlFor="confirmPassword">Confirme contraseña</label>
            <input {...register('confirmPassword')} type="password" required />
          </div>
          <button className="w-full mt-6 boton-default" type="submit">
            Registrarse
          </button>
        </form>
      </div>
    </section>
  );
};

export default SignUp;
