export enum ROUTES {
  SIGNIN = '/sign-in',
  SIGNUP = '/sign-up',
  MOVIES = '/movies',
  USERS = '/users',
}
