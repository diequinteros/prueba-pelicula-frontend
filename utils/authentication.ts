import { IUser } from '@/types/models';

const key = 'authentication';

interface IAuthentication {
  token: string;
  role: IUser['data']['role'];
  email: string;
}

export const persistAuthentication = (authentication: IAuthentication) => {
  const stringyfiedData = JSON.stringify(authentication);

  localStorage.setItem(key, stringyfiedData);
};

export const retrieveAuthentication = (): IAuthentication | undefined => {
  const stringyfiedData = localStorage.getItem(key);

  if (stringyfiedData) {
    const data = JSON.parse(stringyfiedData);

    return data;
  }
};

export const deleteAuthentication = () => {
  localStorage.removeItem(key);
};
