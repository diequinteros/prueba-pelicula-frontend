import { AppRouterInstance } from 'next/dist/shared/lib/app-router-context.shared-runtime';
import { ROUTES } from './constants';

const handleExpired = (
  response: { error: string; code?: number },
  router: AppRouterInstance,
) => {
  if (response.code !== undefined && response.code === 401) {
    router.push(ROUTES.SIGNIN);
  }
};

export default handleExpired;
