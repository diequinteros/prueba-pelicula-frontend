'use client';
import {
  deleteAuthentication,
  retrieveAuthentication,
} from '@/utils/authentication';
import { ROUTES } from '@/utils/constants';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import React, { useEffect, useState } from 'react';

const Header = () => {
  const [authenticated, setAuthenticated] = useState(false);
  const pathname = usePathname();

  useEffect(() => {
    if (retrieveAuthentication()) {
      setAuthenticated(true);
    } else {
      setAuthenticated(false);
    }
    console.log('route changed');
  }, [pathname]);

  const handleSignOut = () => {
    deleteAuthentication();
    setAuthenticated(false);
  };

  return (
    <div className="h-32 p-8 flex flex-row justify-between items-center bg-theme-dark-red text-white shadow-lg shadow-stone-950">
      <Link href={'/'}>
        <h1 className="font-bold">Prueba peliculas</h1>
      </Link>
      <div className="flex flex-row gap-6 justify-end items-center">
        <Link className="hover:underline" href={ROUTES.MOVIES}>
          Peliculas
        </Link>
        {authenticated ? (
          <>
            {retrieveAuthentication()?.role === 'admin' && (
              <Link className="hover:underline" href={ROUTES.USERS}>
                Usuarios
              </Link>
            )}
            <a
              className="hover:underline cursor-pointer"
              onClick={handleSignOut}
            >
              Cerrar sesion
            </a>
          </>
        ) : (
          <>
            <Link className="hover:underline" href={ROUTES.SIGNIN}>
              Iniciar sesion
            </Link>
            <Link className="boton-secundario" href={ROUTES.SIGNUP}>
              Registrarse
            </Link>
          </>
        )}
      </div>
    </div>
  );
};

export default Header;
