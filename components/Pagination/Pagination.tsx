import Link from 'next/link';
import React from 'react';
import { MdNavigateBefore, MdNavigateNext } from 'react-icons/md';

interface IProperties {
  currentPage: number;
  totalPages: number;
  route: string;
}

const Pagination = ({ currentPage, totalPages, route }: IProperties) => {
  const getPagesToRender = () => {
    let pagesToRender = [];

    let index = -3;

    while (pagesToRender.length < 7) {
      const newPage = currentPage + index;
      if (newPage <= totalPages) {
        if (newPage > 0) {
          pagesToRender.push(newPage);
        }
        index++;
      } else {
        break;
      }
    }

    while (pagesToRender.length < 7 && pagesToRender[0] !== 1) {
      pagesToRender = [pagesToRender[0] - 1, ...pagesToRender];
    }
    return pagesToRender;
  };

  return (
    totalPages > 1 && (
      <section className="flex flex-row border border-black/10 rounded-md overflow-hidden w-fit">
        <Link
          href={`${route}${currentPage === 1 ? 1 : currentPage - 1}`}
          className={`rounded-none p-1 h-fit w-fit text-center border-r bg-stone-800 text-white hover:bg-theme-dark-red/50 border-black/10`}
        >
          <MdNavigateBefore size={'1.5rem'} />
        </Link>
        {getPagesToRender().map((page, index) => {
          return (
            <Link
              href={`${route}${page}`}
              className={`rounded-none p-1 aspect-square w-8 text-center ${
                index === 0 ? '' : 'border-l'
              } ${
                page === currentPage
                  ? ' bg-theme-dark-red text-white'
                  : 'bg-stone-800 text-white hover:bg-theme-dark-red/50'
              } border-black/10`}
              key={page}
            >
              {page}
            </Link>
          );
        })}
        <Link
          href={`${route}${
            currentPage === totalPages ? currentPage : currentPage + 1
          }`}
          className={`rounded-none p-1 h-fit w-fit text-center border-l bg-stone-800 text-white hover:bg-theme-dark-red/50 border-black/10`}
        >
          <MdNavigateNext size={'1.5rem'} />
        </Link>
      </section>
    )
  );
};

export default Pagination;
