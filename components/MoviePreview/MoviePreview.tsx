'use client';
import Image from 'next/image';
import React, { useState } from 'react';

interface IProperties {
  title: string;
  image: string;
  likes: number;
}

const MoviePreview = ({ title, image, likes }: IProperties) => {
  const [hovered, setHovered] = useState(false);

  const handleMouseEnter = () => {
    setHovered(true);
  };

  const handleMouseLeave = () => {
    setHovered(false);
  };

  return (
    <div
      className=" aspect-[2/3] w-full cursor-pointer"
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      <div className="relative ">
        <div
          className={`bg-stone-900/60 ${hovered ? 'absolute' : 'absolute lg:hidden'} bottom-0 w-full p-2`}
        >
          <span>{likes} Me gusta</span>
        </div>
        <Image
          className=" object-cover w-full"
          src={image}
          alt={`Movie poster of ${title}`}
          width={200}
          height={400}
        />
      </div>
      <p className=" leading-4 max-h-8 break-words overflow-hidden text-center">
        {title}
      </p>
    </div>
  );
};

export default MoviePreview;
