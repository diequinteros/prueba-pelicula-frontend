'use client';
import useCloseContextMenu from '@/hooks/useCloseContextMenu';
import React, { ReactNode, useRef } from 'react';

interface IProperties {
  show: boolean;
  title: string;
  message: ReactNode;
  onAccept: () => void;
  onCancel?: () => void;
  onClickOutside?: () => void;
}

const Alert = ({
  show,
  title,
  message,
  onAccept,
  onCancel,
  onClickOutside,
}: IProperties) => {
  const alertReference = useRef<HTMLDivElement>(null);
  useCloseContextMenu(alertReference, onClickOutside ?? (() => {}));
  return (
    show && (
      <div className="theme-full-dark-blur">
        <div className="w-full h-full flex justify-center items-center">
          <div
            className="bg-stone-800 border-2 border-stone-700 rounded-md overflow-hidden max-w-lg min-w-96 shadow-lg shadow-black/10 p-4"
            ref={alertReference}
          >
            <div className="bg-theme-orange p-2">
              <span className="font-bold text-white text-xl">{title}</span>
            </div>
            <div className="p-2">{message}</div>
            <div className="flex flex-row gap-2 justify-end p-2">
              <button onClick={onAccept} className="boton-default">
                Aceptar
              </button>
              {onCancel === undefined ? undefined : (
                <button onClick={onCancel} className="boton-secundario">
                  Cancelar
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    )
  );
};

export default Alert;
